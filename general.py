import discord
import time
from discord.ext import commands


class General:
    """General Dev Bot Commands"""

    def __init__(self, bot):
        self.bot = bot


    @commands.command()
    async def ping(self):
        """Ping Command"""
        await self.bot.say(':ping_pong: Pong!')

    @commands.command(pass_context=True)
    async def pingtime(self,ctx):
        """Ping Time"""
        channel = ctx.message.channel
        t1 = time.perf_counter()
        await self.bot.send_typing(channel)
        t2 = time.perf_counter()
        await self.bot.say("Ping Time: {}ms".format(round((t2-t1)*1000)))

    @commands.command(pass_context=True)
    async def giveaway(self,em):
    	"""Enter The Gift Card Giveaway!"""
    	em = discord.Embed(description='YOU HAVE BEEN ENTERED INTO THE GIFTCARD GIVEAWAY!', colour=0xFF0000)
    	em.set_author(name='GIVEAWAY BOT', url='https://noodler.co/')
    	em.set_thumbnail(url='http://isahotel.com.au/wp-content/uploads/2016/02/Gift_Card.jpg')
    	await self.bot.say(embed=em)

    @commands.command(pass_context=True)
    async def avatar(self, ctx, *, user: discord.Member=None):
        """Get user's avatar URL."""
        author = ctx.message.author

        if not user:
            user = author

        avatar = user.avatar_url
        await self.bot.say("{}'s Avatar URL : {}".format(user.name, avatar))



def setup(bot):
    bot.add_cog(General(bot))





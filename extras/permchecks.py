from discord.ext import commands
import discord.utils

def is_owner_check(message):
    return message.author.id == '102341036403068928' or '211098424655740929'

def is_owner():
    return commands.check(lambda ctx: is_owner_check(ctx.message))

def check_permissions(ctx, perms):
    msg = ctx.message
    if is_owner_check(msg):
        return True

    ch = msg.channel
    author = msg.author
    resolved = ch.permissions_for(author)
    return all(getattr(resolved, name, None) == value for name, value in perms.items())

#not sure if these will vork
def mod_or_permissions(**perms):
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.name in ('DevBot Mod', 'DevBot Admin'), **perms)

    return commands.check(predicate)

def admin_or_permissions(**perms):
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.name == 'DevBot Admin', **perms)

    return commands.check(predicate)